(local marks
        {
        :home
       [
         {:category "home"
          :name "home"
          :uri "/home/erik"}
         {:category "home"
          :name "bin"
          :uri "/home/erik/bin"}
         {:category "home"
          :name "Downloads"
          :uri "/home/erik/Downloads"}
         {:category "home"
          :name "gemini"
          :uri "/home/erik/gemini"}
         {:category "home"
          :name "scripts"
          :uri "/home/erik/scripts"}
         {:category "home"
          :name "scratchpads"
          :uri "/home/erik/scratchpads"}
         {:category "home"
          :name "cprogs"
          :uri "/home/erik/cProgs/"}
         ]

        :projects
        [
         {:category "projects"
          :name "javaMailReader_jar"
          :uri "/home/erik/git/java/javamailreader/out/artifacts/javaMailReader_jar"}
         {:category "projects"
          :name "fnlfetch"
          :uri "/home/erik/git/projects/lisp/fennel/fnlfetch"}
         {:category "projects"
          :name "hydra"
          :uri "/home/erik/git/projects/www/myWebStuff/hydra"}
         {:category "projects"
          :name "suckless"
          :uri "/home/erik/cProgs/suckless"}
         ]

        :minecraft
        [
         {:category "minecraft"
          :name "polyMC"
          :uri "/home/erik/.local/share/polymc"}
         {:category "minecraft"
          :name "minecraftStuff"
          :uri "/media/erik/minecraftStuff"}
         ]

        :media
        [
         {:category "media"
          :name "DoctorWho"
          :uri "/media/erik/media/tvshows/DoctorWho"}
         {:category "media"
          :name "marvel"
          :uri "/media/erik/media/tvshows/disney/marvel"}
         ]

        :dotfiles
        [
         {:category "dotfiles"
          :name "dotfiles"
          :uri "/home/erik/.config"}
         {:category "dotfiles"
          :name "awesome"
          :uri "/home/erik/.config/awesome"}
         {:category "dotfiles"
          :name "xplr"
          :uri "/home/erik/.config/xplr"}
         {:category "dotfiles"
          :name "luakit"
          :uri "/home/erik/.config/luakit"}
         {:category "dotfiles"
          :name "wezterm"
          :uri "/home/erik/.config/wezterm"}
         {:category "dotfiles"
          :name "key-mapper"
          :uri "/home/erik/.config/key-mapper"}
         {:category "dotfiles"
          :name "emacs"
          :uri "/home/erik/.config/rational-emacs"}
         ]

        }
       )
marks
