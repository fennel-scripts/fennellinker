#!/bin/env fennel
;;(local lfs     (require :lfs   ) )
(local marks   (require :marks ) )

(local lfs {})

(fn lfs.mkdir [dir]
	(print (.. "mkdir -p " dir "\n"))
)
(fn lfs.rmdir [dir]
	(print (.. "rmdir --ignore-fail-on-non-empty " dir "\n"))
)

(fn lfs.link [src tgt symlink]
	(local mksym (if symlink " -s " " "))
	(print (.. "ln " mksym " " src " " tgt "\n"))
)

(local categories
         [
          :home       ;;"home"
          :projects   ;;"projects"
          :dotfiles   ;;"dotfiles"
          ]
         )
(if (= (os.getenv "HOST") "ArcoLinuxPC")
  (table.insert categories :media)
  (table.insert categories :minecraft)
  )

(local dir "/home/erik/.config/bookmarks/")

;;(lfs.link old new true)

;;lfs.link (old, new[, symlink])

(fn main []
  (each [key value (ipairs categories)]
    (lfs.rmdir (.. dir key))
    (lfs.mkdir (.. dir value))
    (local sep "#===============")
    (print (.. "#<[" sep value sep "]>"))
    (each [k v (pairs (. marks value))]
      (lfs.link  (. v :uri)  (.. dir value "/" (. v :name)) true)
      (print "#" (. v :uri)  (.. dir value "/" (. v :name)))
      )
    )
  )

(main)
